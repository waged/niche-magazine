package tv.nichemagazine.nichemagazine;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.Toast;

import com.nipunbirla.boxloader.BoxLoaderView;


public class MainActivity extends AppCompatActivity {

    public static String FACEBOOK_URL = "https://www.facebook.com/NicheMagazineEgypt/";
    public static String FACEBOOK_PAGE_ID = "NicheMagazineEgypt";
    String host,scheme ;
    String wordPress;
    BoxLoaderView boxLoader;
    WebView htmlWebView ;
    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         boxLoader = (BoxLoaderView) findViewById(R.id.progress);
          imageView = (ImageView) findViewById(R.id.imageview);
         htmlWebView = (WebView) findViewById(R.id.webview);

        if (hasConnection()) {
            Toast.makeText(this, "Please wait ...", Toast.LENGTH_SHORT).show();
            imageView.setVisibility(View.GONE);
            htmlWebView.setWebViewClient(new CustomWebViewClient());
            WebSettings webSetting = htmlWebView.getSettings();
            webSetting.setJavaScriptEnabled(true);
            webSetting.setJavaScriptCanOpenWindowsAutomatically(false);
            webSetting.setSupportMultipleWindows (false);
            webSetting.setSupportZoom (false);
            wordPress = getResources().getString(R.string.word_press);
            if(savedInstanceState == null){
            htmlWebView.loadUrl(wordPress);
            }

        }
        else{
            boxLoader.setVisibility(View.INVISIBLE);
            imageView.setVisibility(View.VISIBLE);
            Toast.makeText(this, "Check Connection and Try again!", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    protected void onSaveInstanceState(Bundle outState )
    {
        super.onSaveInstanceState(outState);
        htmlWebView.saveState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);
        htmlWebView.restoreState(savedInstanceState);
    }
    @Override
    public void onBackPressed() {
        if (htmlWebView.canGoBack())
            htmlWebView.goBack();
        else
            super.onBackPressed();
    }

    public boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            return true;
        }

        return false;
    }

   public class CustomWebViewClient extends WebViewClient {

       @SuppressWarnings("deprecation")
       @Override
       public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
           htmlWebView.setVisibility(View.INVISIBLE);
           imageView.setImageResource(R.drawable.noconnection);
           imageView.setVisibility(View.VISIBLE);
       }

        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.e("OverrideUrlLoading","url :"+url);
            final Uri uri = Uri.parse(url);
            return handleUri(uri);
        }

       @Override
       public void onPageStarted(WebView view, String url, Bitmap favicon) {
           Log.e("onPageStarted","changed: "+ url);
           boxLoader.setVisibility(View.VISIBLE);
       }

       @Override
       public void onPageFinished(WebView view, String url) {
           Log.e("onPageFinished","changed: "+ url);
           boxLoader.setVisibility(View.INVISIBLE);
       }


        private boolean handleUri(Uri uri) {
            Log.e("PATH", "Uri =" + uri);
                host = uri.getHost();
                scheme = uri.getScheme();
            if (scheme.contains("facebook")|| host.contains("www.facebook.com")) {
                     Log.e("handleUri","changed: "+ uri);
                    Toast.makeText(MainActivity.this, "Loading Facebook..", Toast.LENGTH_SHORT).show();
                    Intent facebookIntent = new Intent(Intent.ACTION_VIEW);
                    String facebookUrl = getFacebookPageURL(MainActivity.this);
                    facebookIntent.setData(Uri.parse(facebookUrl));
                    try {
                        startActivity(facebookIntent);
                    } catch (ActivityNotFoundException e) {
                        startActivity(new Intent(Intent.ACTION_VIEW,
                                Uri.parse("https://www.facebook.com/NicheMagazineEgypt/")));
                    }
                return true;
            }
            if (scheme.contains("instagram.com/niche.magazine")|| host.contains("instagram")){
                Log.e("handleUri","changed: "+ uri);
                 uri = Uri.parse("https://www.instagram.com/niche.magazine/");
                Intent instagram = new Intent(Intent.ACTION_VIEW, uri);
                instagram.setPackage("com.instagram.android");
                try {
                    startActivity(instagram);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://www.instagram.com/niche.magazine/")));
                }

                return true;
            }
            if (scheme.contains("twitter")|| host.contains("twitter")){
                Log.e("handleUri","changed: "+ uri);
                startActivity(new Intent(Intent.ACTION_VIEW,
                        //com.twitter.android
                         Uri.parse("https://www.twitter.com/")));
                return true;
            }
            if (scheme.contains("pinterest")|| host.contains("pinterest")){
                Log.e("handleUri","changed: "+ uri);
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://www.pinterest.com/")));
                return true;
            }

            return false;
        }
    }
    //method to get the right URL to use in the intent
    public String getFacebookPageURL(Context context) {
        PackageManager packageManager = context.getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) { //newer versions of fb app
                return "fb://facewebmodal/f?href=" + FACEBOOK_URL;
            } else { //older versions of fb app
                return "fb://page/" + FACEBOOK_PAGE_ID;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return FACEBOOK_URL; //normal web url
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Check if the key event was the Back button and if there's history
        if ((keyCode == KeyEvent.KEYCODE_BACK) && htmlWebView.canGoBack()) {
            htmlWebView.goBack();
            return true;
        }
        /*If it wasn't the Back key or there's no web page history, bubble up to the default
        system behavior (probably exit the activity)*/
        return super.onKeyDown(keyCode, event);
    }
}